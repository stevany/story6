$(document).ready(function () {
    $("#accordion").accordion({
        collapsible: true,
        active: false
    });

    var local = window.localStorage;

    if (local.getItem('purpleMode') == 'true') {
        $('#main').removeClass('main-container');
        $('#main').addClass('main-container-change');
        $('#judul_utama').removeClass('judul-utama');
        $('#judul_utama').addClass('judul-utama-change');
        $('#sub_judul').removeClass('sub-judul');
        $('#sub_judul').addClass('sub-judul-change');
        $('#id_submit').removeClass('submit');
        $('#id_submit').addClass('submit-change');
        $('#change_theme_button').removeClass('submit');
        $('#change_theme_button').addClass('submit-change');
        $('.form-group > label').css('color', 'white');
    } else {
        $('#main').addClass('main-container');
        $('#main').removeClass('main-container-change');
        $('#judul_utama').addClass('judul-utama');
        $('#judul_utama').removeClass('judul-utama-change');
        $('#sub_judul').addClass('sub-judul');
        $('#sub_judul').removeClass('sub-judul-change');
        $('#id_submit').addClass('submit');
        $('#id_submit').removeClass('submit-change');
        $('#change_theme_button').addClass('submit');
        $('#change_theme_button').removeClass('submit-change');
        $('.form-group > label').css('color', 'black');
    }

    document.getElementById('change_theme_button').onclick = changeTheme;

    function changeTheme() {
        if ($('#main').hasClass('main-container')) {
            $('#main').removeClass('main-container');
            $('#main').addClass('main-container-change');
            $('#judul_utama').removeClass('judul-utama');
            $('#judul_utama').addClass('judul-utama-change');
            $('#sub_judul').removeClass('sub-judul');
            $('#sub_judul').addClass('sub-judul-change');
            $('#id_submit').removeClass('submit');
            $('#id_submit').addClass('submit-change');
            $('#change_theme_button').removeClass('submit');
            $('#change_theme_button').addClass('submit-change');
            $('.form-group > label').css('color', 'white');
            local.setItem('purpleMode', true);
        } else {
            $('#main').addClass('main-container');
            $('#main').removeClass('main-container-change');
            $('#judul_utama').addClass('judul-utama');
            $('#judul_utama').removeClass('judul-utama-change');
            $('#sub_judul').addClass('sub-judul');
            $('#sub_judul').removeClass('sub-judul-change');
            $('#id_submit').addClass('submit');
            $('#id_submit').removeClass('submit-change');
            $('#change_theme_button').addClass('submit');
            $('#change_theme_button').removeClass('submit-change');
            $('.form-group > label').css('color', 'black');
            local.setItem('purpleMode', false);
        }
    }

    $(window).on('load', function () { // makes sure the whole site is loaded
        $('#status').delay(2000).fadeOut(); // will first fade out the loading animation
        $('#preloader').delay(2500).fadeOut('slow'); // will fade out the white DIV that covers the website.
        $('body').delay(2500).css({'overflow': 'visible'});
    })

});

