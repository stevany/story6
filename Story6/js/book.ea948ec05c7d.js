$(document).ready(function () {
    $.ajax({
        method: "GET",
        url: "/favorite/cekFav",
        success: function (hasil) {
            var data = hasil.data; //key return
            for (var x = 0; x < data.length; x++) {
                var dataBook = data[x];
                var title = dataBook.title;
                var name = title.replace(/'/g, "\\");
                if (dataBook.boolean) {
                    var button = '<button' + ' id="' + dataBook.id + '"' + ' class="button_star"' + ' onClick="unFavorite(\'' + dataBook.id + '\')">' + '<i class="fav_star"></i></button>';
                } else {
                    var button = '<button'+ ' id="' + dataBook.id + '"' + ' class="button_star"' + ' onClick="Favorite(\'' + name + '\', \'' + dataBook.id + '\')">' +'<i class="unfav_star"></i></button>';
                }
                var html = '<tr class="table">' + '<td>' + title + '</td>' + '<td>' + dataBook.authors + '</td>' +
                    '<td>' + dataBook.published + '</td>' + '<td>' + button + '</td>' + '</tr>';
                $('tbody').append(html);

            }
        },
         error: function (error) {
            alert("Halaman buku tidak ditemukan")
        }
    })
});

var Favorite = function (title, id) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: "POST",
        url: "/favorite/",
        headers:{
        "X-CSRFToken": csrftoken
    },
        data: {title: title, id: id},
        success: function (hasil) {
        var button = '<button' + ' id="' + hasil.id + '"' + ' class="button_star"' + ' onClick="unFavorite(\'' + hasil.id + '\')">' +'<i class="fav_star"></i></button>';
            $("#"+hasil.id).replaceWith(button);
            $(".count").replaceWith("<span class='count'>" + hasil.count + "</span>")
        },
        error: function (error) {
            alert("Error, cannot get data from server")
        }
    });
};
var unFavorite = function (id) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: "POST",
        url: "/favorite/unfavorite/s",
        headers:{
        "X-CSRFToken": csrftoken
    },
        data: {id: id},
        success: function (count) {
            var button = '<button' + ' id="' + id + '"' + ' class="button_star"' + ' onClick="Favorite(\'' + count.name.replace(/'/g, "\\'") + '\', \'' + id + '\')">' +'<i class="unfav_star"></i></button>';
            $("tbody").find("#"+id).replaceWith(button);
            $(".count").replaceWith("<span class='count'>" + count.count + "</span>");
            // $("#"+id).remove();
        },
        error: function (error) {
            alert("Error, cannot get data from server")
        }
    });
};
