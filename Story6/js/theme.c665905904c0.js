$(document).ready(function () {
    document.getElementById('change_theme_button').onclick = changeTheme;

    function changeTheme() {
        if ($('#main').hasClass('main-container')) {
            $('#main').removeClass('main-container');
            $('#main').addClass('main-container-change');
            $('#judul_utama').removeClass('judul-utama');
            $('#judul_utama').addClass('judul-utama-change');
            $('#sub_judul').removeClass('sub-judul');
            $('#sub_judul').addClass('sub-judul-change');
        } else {
            $('#main').addClass('main-container');
            $('#main').removeClass('main-container-change');
            $('#judul_utama').addClass('judul-utama');
            $('#judul_utama').removeClass('judul-utama-change');
            $('#sub_judul').addClass('sub-judul');
            $('#sub_judul').removeClass('sub-judul-change');
        }
    }
});