$(function () {
    $("#accordion").accordion({
        collapsible: true,
        active: false
    });

    document.getElementById('change_theme_button').onclick = changeTheme;

    function changeTheme() {
        window.alert('Test')
        if ($('#main').hasClass('main-container')) {
            $('#main').removeClass('main-container');
            $('#main').addClass('main-container-change');
            $('#judul_utama').removeClass('judul-utama');
            $('#judul_utama').addClass('judul-utama-change');
            $('#sub_judul').removeClass('sub-judul');
            $('#sub_judul').addClass('sub-judul-change');
            $('#id_submit').removeClass('submit');
            $('#id_submit').addClass('submit-change');
            $('#change_theme_button').removeClass('submit');
            $('#change_theme_button').addClass('submit-change');
            $('.form-group > label').css('color', 'white');
        } else {
            $('#main').addClass('main-container');
            $('#main').removeClass('main-container-change');
            $('#judul_utama').addClass('judul-utama');
            $('#judul_utama').removeClass('judul-utama-change');
            $('#sub_judul').addClass('sub-judul');
            $('#sub_judul').removeClass('sub-judul-change');
            $('#change_theme_button').addClass('submit');
            $('#change_theme_button').removeClass('submit-change');
            $('.form-group > label').css('color', 'black');
        }
    }

});

