from django.db import models

# Create your models here.

class DataSubscribe (models.Model) :
    nama = models.CharField(max_length=30)
    email = models.CharField(max_length=30, unique=True, primary_key = True)
    password = models.CharField(max_length=10)