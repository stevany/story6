from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import subscribe
from .models import DataSubscribe
from django.db import IntegrityError


# Create your tests here.

class SubscribeUnitTest(TestCase):
    def test_url_page_is_exist(self):
        response = Client().get('/subscribe/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_function(self):
        found = resolve('/subscribe/')
        self.assertEqual(found.func, subscribe)

    def test_model_can_create_object(self):
        DataSubscribe.objects.create(nama='hany', email="hany@gmail.com", password="abcde")
        counting_all_input = DataSubscribe.objects.all().count()
        self.assertEqual(counting_all_input, 1)

    def test_nama_max_length(self):
        nama = DataSubscribe.objects.create(nama="paling banyak 30 karakter ya")
        self.assertLessEqual(len(str(nama)), 30)

    def test_unique_email(self):
        DataSubscribe.objects.create(email="hany@email.com")
        with self.assertRaises(IntegrityError):
            DataSubscribe.objects.create(email="hany@email.com")


    def test_check_email_view_get_return_200(self):
        email = "hany@gmail.com"
        Client().post('/subscribe/checkEmail', {'email': email})
        response = Client().post('/subscribe/', {'email': 'hany@gmail.com'})
        self.assertEqual(response.status_code, 200)

    def test_check_email_already_exist_view_get_return_200(self):
        DataSubscribe.objects.create(nama="hany",
                                      email="hany@gmail.com",
                                      password="abcde")
        response = Client().post('/subscribe/checkEmail', {
            "email": "hany@gmail.com"
        })
        self.assertEqual(response.json()['is_email'], True)

    def test_subscribe_should_return_status_subscribe_true(self):
        response = Client().post('/subscribe/', {
            "email": "hany@gmail.com",
            "nama": "Hany",
            "password":  "password",
        })
        self.assertEqual(response.json()['status_subscribe'], True)

    def test_subscribe_should_return_status_subscribe_false(self):
        nama, email, password = "Hany", "hany@gmail.com", "password"
        DataSubscribe.objects.create(nama=nama, email=email, password=password)
        response = Client().post('/subscribe/', {
            "email": email,
            "nama": nama,
            "password":  password,
        })
        self.assertEqual(response.json()['status_subscribe'], False)


