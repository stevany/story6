from django.urls import path
from .views import subscribe, checkEmail, getDataSubscribe, result_subscribe, delete_from_model

urlpatterns = [
    path('', subscribe, name="subscribe"),
    path('checkEmail', checkEmail, name="checkEmail"),
    path('getDataSubscribe/', getDataSubscribe, name="getDataSubscribe"),
    path('result/', result_subscribe, name="result"),
    path('delete/', delete_from_model, name='delete'),
]
