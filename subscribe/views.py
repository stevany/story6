from django.shortcuts import render
from django.http import JsonResponse
from .forms import FormSubscribe
from .models import DataSubscribe


# Create your views here.

def subscribe(request):
    form = FormSubscribe(request.POST)
    if request.method == 'POST' and form.is_valid():
        data = form.cleaned_data
        status_subscribe = True
        try:
            DataSubscribe.objects.create(**data)
        except:
            status_subscribe = False
        return JsonResponse({'status_subscribe': status_subscribe})
    content = {'form': form}
    return render(request, 'subscribe.html', content)


def checkEmail(request):
    if request.method == "POST":
        email = request.POST['email']
        is_email_already_exist = DataSubscribe.objects.filter(pk=email).exists()
        return JsonResponse({'is_email': is_email_already_exist})


def getDataSubscribe(request):
    if request.method == 'POST':
        dataSubscribe = DataSubscribe.objects.all().values()
        listDataSubscribe = list(dataSubscribe)
        return JsonResponse({'listDataSubscribe': listDataSubscribe})


def result_subscribe(request):
    return render(request, 'dataSubscribe.html')


def delete_from_model(request):
    if request.method == 'POST':
        email = request.POST['email']
        DataSubscribe.objects.filter(pk=email).delete()
        now = list(DataSubscribe.objects.all().values())
        return JsonResponse({'deleted': True, 'now': now})
