from django import forms

class FormSubscribe(forms.Form):
    nama = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs={'placeholder': 'Nama Lengkap', 'id' : 'nama'}))
    email = forms.CharField(required=True, max_length=30,
                            widget=forms.DateInput(attrs={'type': 'email', 'placeholder': 'Email', 'id' : 'email'}))
    password = forms.CharField(required=True, max_length=10,
                               widget=forms.TextInput(attrs={'type': 'password', 'placeholder': 'Password', 'id' : 'password'}))

