from django.shortcuts import render
from django.http import HttpResponseRedirect

# Create your views here.

def profil(request):
    return render(request, 'profil.html')