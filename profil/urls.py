from django.urls import path
from .views import profil as profil

urlpatterns = [
    path('', profil, name="profil")
]
