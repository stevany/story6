from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import profil as profil
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class ChallengeUnitTest(TestCase):

    def test_url_page_is_exist(self):
        response = Client().get('/profil/')
        self.assertEqual(response.status_code, 200)

    def test_challenge_using_index_function(self):
        found = resolve('/profil/')
        self.assertEqual(found.func, profil)

    def test_text_is_exist(self):
        response = Client().get('/profil/')
        html_response = response.content.decode('utf8')
        self.assertIn('Stevany', html_response)

    def test_template_is_used(self):
        response = Client().get('/profil/')
        self.assertTemplateUsed(response, 'profil.html')

class HomeFunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(HomeFunctionalTest, self).setUp()

    def test_title_layout(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('https://stevany-love-ppw.herokuapp.com/profil/')
        # Find the title element
        main_title = selenium.find_element_by_id('main_title').text
        sub_title = selenium.find_element_by_id('sub_title').text
        header = selenium.find_element_by_id('header').text
        time.sleep(5)
        self.assertIn(main_title, header)
        self.assertIn(sub_title, header)

    def test_image_layout(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('https://stevany-love-ppw.herokuapp.com/profil/')
        # Find the image
        hany_picture = selenium.find_element_by_id('hany_picture').text
        header = selenium.find_element_by_id('header').text
        time.sleep(5)
        self.assertIn(hany_picture, header)


    def test_img_styling(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('https://stevany-love-ppw.herokuapp.com/profil/')
        # Find the image
        img_hany = selenium.find_element_by_id('hany_picture').get_attribute('class')
        time.sleep(5)
        self.assertEqual(img_hany, 'img_hany')

    def test_text_styling(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('https://stevany-love-ppw.herokuapp.com/profil/')
        # Find the text
        title1 = selenium.find_element_by_id('main_title').get_attribute('class')
        title2 = selenium.find_element_by_id('sub_title').get_attribute('class')
        time.sleep(5)
        self.assertEqual(title1, 'title1')
        self.assertEqual(title2, 'title2')

    def test_accordion(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/profil/')
        # Find the accordion section 1
        button_accordion = selenium.find_element_by_id('ui-id-1')
        section1 = selenium.find_element_by_id('ui-id-1').get_attribute('aria-selected')
        isi_section1 = selenium.find_element_by_id('ui-id-2').get_attribute('aria-hidden')
        button_accordion.send_keys(Keys.RETURN)
        self.assertNotEquals(section1,isi_section1)


    def tearDown(self):
        time.sleep(5)
        self.selenium.quit()
        super(HomeFunctionalTest, self).tearDown()
