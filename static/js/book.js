$(document).ready(function () {
    $('.kategori').click(function () {
        //Ambil ID
        var id =  this.id;
        $.ajax({
            method: "GET",
            url: "/favorite/cekFav?q=" + id, // url yang akan dipanggil
            success: function (hasil) {
                // disini akan berisi code yang akan dieksekusi jika url berhasil dipanggil
                // hasil merupakan data yang diperoleh dari url
                var data = hasil.data; //key return
                $('td').remove(html);
                for (var x = 0; x < data.length; x++) {
                    var dataBook = data[x];
                    var title = dataBook.title;
                    if (dataBook.boolean) {
                        var button = '<button' + ' id="' + dataBook.id + '"' + ' class="button_star"' +
                            ' onClick="unFavorite(\'' + dataBook.id + '\')">' + '<i class="fav_star"></i></button>';
                    } else {
                        var button = '<button' + ' id="' + dataBook.id + '"' + ' class="button_star"' +
                            ' onClick="Favorite(\'' + dataBook.id + '\')">' +
                            '<i class="unfav_star"></i></button>';
                    }
                    var html = '<tr class="table">' + '<td>' + title + '</td>' + '<td>' + dataBook.authors + '</td>' +
                        '<td>' + dataBook.published + '</td>' + '<td>' + button + '</td>' + '</tr>';
                    $('tbody').append(html);

                }
            },
            error: function (error) {
                // disini akan berisi code yang akan dieksekusi jika example.com gagal dipanggil
                alert("Halaman buku tidak ditemukan")
            }
        })

    });

});

var Favorite = function (id) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: "POST",
        url: "/favorite/",
        headers: {
            "X-CSRFToken": csrftoken
        },
        data: {id: id},
        success: function (hasil) {
            var button = '<button' + ' id="' + hasil.id + '"' + ' class="button_star"' +
                ' onClick="unFavorite(\'' + hasil.id + '\')">' + '<i class="fav_star"></i></button>';
            $("#" + hasil.id).replaceWith(button);
            $("#counter").replaceWith("<span id='counter'>" + hasil.count + "</span>")
        },
        error: function (error) {
            alert("Error, cannot get data from server")
        }
    });
};

var unFavorite = function (id) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: "POST",
        url: "/favorite/unfavorite/",
        headers: {
            "X-CSRFToken": csrftoken
        },
        data: {id: id},
        success: function (count) {
            var button = '<button' + ' id="' + id + '"' + ' class="button_star"' +
                ' onClick="Favorite(\'' + id + '\')">' +
                '<i class="unfav_star"></i></button>';
            $("tbody").find("#" + id).replaceWith(button);
            $("#counter").replaceWith("<span id='counter'>" + count.count + "</span>");
        },
        error: function (error) {
            alert("Error, cannot get data from server")
        }
    });
};

$(document).ready(function () {
       gapi.load('auth2', function() {
        gapi.auth2.init();
      });
});