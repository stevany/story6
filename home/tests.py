from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import home as home
from .models import InputStatus
from .forms import FormStatus
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


# Create your tests here.
class StoryUnitTest(TestCase):
    def test_url_page_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_story_using_index_function(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_model_can_add_new_status(self):
        InputStatus.objects.create(status='test masuk ke models')
        counting_all_status_message = InputStatus.objects.all().count()
        self.assertEqual(counting_all_status_message, 1)

    def test_if_form_is_blank(self):
        form = FormStatus(data={'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'][0],
            'This field is required.',
        )

    def test_form_post_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/', {'status': test})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_form_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/', {'status': ''})
        self.assertNotEqual(response_post.status_code, 302)

        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_self_func(self):
        InputStatus.objects.create(status='HELLO')
        status = InputStatus.objects.get(id=1)
        self.assertEqual(str(status), status.status)

    def test_text_max_length(self):
        status = InputStatus.objects.create(status="paling banyak 300 karakter ya")
        self.assertLessEqual(len(str(status)), 300)


class HomeFunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(HomeFunctionalTest, self).setUp()

    def test_input(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')
        # Find the form element
        status_message = selenium.find_element_by_id('id_status')
        submit = selenium.find_element_by_id('id_submit')
        time.sleep(5)

        # Fill the form with data
        status_message.send_keys('coba-coba')

        # Submitting the form
        submit.send_keys(Keys.RETURN)

        self.assertIn("Stevany Love PPW", selenium.title)
        self.assertIn("coba-coba", selenium.page_source)

    def test_change_theme(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000')
        # Find the change theme button
        time.sleep(3)
        button_change_theme = selenium.find_element_by_id('change_theme_button')
        # Press change theme button
        button_change_theme.send_keys(Keys.RETURN)
        main_container = selenium.find_element_by_id('main').get_attribute('class')
        self.assertEqual(main_container, 'main-container-change')

    def tearDown(self):
        time.sleep(5)
        self.selenium.quit()
        super(HomeFunctionalTest, self).tearDown()
