from django import forms


class FormStatus(forms.Form):
    status = forms.CharField(label='Status', required=True, max_length=300, widget=forms.TextInput)


    error_messages = {
        'required': 'Tolong isi input ini!',
    }
