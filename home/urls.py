from django.urls import path
from .views import home as home

urlpatterns = [
    path('', home, name="home")
]
