from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import FormStatus
from .models import InputStatus

# Create your views here.

def home(request):
    form = FormStatus(request.POST)
    if request.method == 'POST' and form.is_valid():
        data = form.cleaned_data
        InputStatus.objects.create(**data)
        return HttpResponseRedirect('/')

    list_status = list(InputStatus.objects.all())
    list_status.reverse()
    content = {'form': form, 'result': list_status}
    return render(request, 'home.html', content)
