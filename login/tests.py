from django.test import TestCase
from django.test import Client


# Create your tests here.

class Lab11UnitTest(TestCase):

    def test_lab11_not_logged_landing_url_is_exist(self):
        response = Client().get('/login')
        self.assertEqual(response.status_code, 200)

    def test_lab11_false_token(self):
        response = Client().post('/login', {'id_token': "testtt"}).json()
        self.assertEqual(response['status'], "1")

    def test_lab11_right_token(self):
        response = Client().post('/login', {'id_token': "eyJhbGciOiJSUzI1NiIsImtpZCI6IjQ2M2ZlNDgwYzNjNTgzOWJiYjE1ODYxZTA4YzMyZDE4N2ZhZjlhNTYiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiYXpwIjoiNjgzNDc0MDM3ODQ5LWwyZTYxdTl1c3QyM3Vva3RlcnNtNTBzdXFjbGMzbG9uLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiYXVkIjoiNjgzNDc0MDM3ODQ5LWwyZTYxdTl1c3QyM3Vva3RlcnNtNTBzdXFjbGMzbG9uLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTAzNzY5OTQ5MTU1OTgyNjY2NDI5IiwiZW1haWwiOiJzdGV2YW55c3VwYXJkaUBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXRfaGFzaCI6IlBaYUtyN1hqakVDSWJUbVlXYllRU1EiLCJuYW1lIjoic3RldmFueSBzdXBhcmRpIiwicGljdHVyZSI6Imh0dHBzOi8vbGgzLmdvb2dsZXVzZXJjb250ZW50LmNvbS8tYnR1WmthQWp1YjAvQUFBQUFBQUFBQUkvQUFBQUFBQUFCT00vODhwZU1rUHVJSVEvczk2LWMvcGhvdG8uanBnIiwiZ2l2ZW5fbmFtZSI6InN0ZXZhbnkiLCJmYW1pbHlfbmFtZSI6InN1cGFyZGkiLCJsb2NhbGUiOiJlbiIsImlhdCI6MTU0MzQ2NDM1MiwiZXhwIjoxNTQzNDY3OTUyLCJqdGkiOiJmOTlhMDc2OTZhMmZkYWYwNjUyMTFkYTNhYjIwYzMwYmJhMzM0NjRmIn0.OWFSxISRQxrdzVj6xXsfS2blPnfOfBsqCSWgpi0X5b016UJ0Ti1tA8VFZ_05tbAeMKUn5zzF4qXMVArkSbCJiJYK_NfcfgO55tPUHJY6XWBAifGHXs_5l1S6Ssx_-m7xw80jt7iIbxUZ1SQbete9KK5DIQ0ufDkcRX9ESB982EwqiKzf87oI-JgTSzb87bXJ2BwzJOtz-gbcikOVs-PA744P4-nbMHVD_a6FaxbMnmMlHYvP4q5s3_sYZVC2L3U5pVvKlgRTWHDOtLmkuWwvrJ9UXbXtJ2JXqRXvGzW5G_jQDGuc4Y8q1o_rPMMrYeNHU1HChMndALpyCWECJNLD2A"}).json()
        self.assertEqual(response['status'], "0")

    def test_logout(self):
        response = Client().get('/logout')
        self.assertEqual(response.status_code, 302)

