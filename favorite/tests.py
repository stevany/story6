from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import favorite
from django.conf import settings
from importlib import import_module
import requests

# Create your tests here.

class SessionTestCase(TestCase) :
    def set_up(self):
        settings.SESSION_ENGINE = 'django.contrib.sessions.backends.file'
        engine = import_module(settings.SESSION_ENGINE)
        store = engine.SessionStore()
        store.save()
        self.session = store
        self.client.cookies[settings.SESSION_COOKIE_NAME] = store.session_key

class FavoriteUnitTest(SessionTestCase):
    def test_favorite_logged_in(self):
        c = self.client.session
        c['user_id']='test'
        c['name']='hany'
        c.save()
        response = Client().get('/favorite/')
        self.assertEqual(response.status_code, 302)

    def test_logged_in_page_is_exist(self):
        response = Client().get('/favorite/')
        self.assertEqual(response.status_code, 302)

    def test_using_index_func(self):
        found = resolve('/favorite/')
        self.assertEqual(found.func, favorite)

    def test_client_can_POST_and_render(self):
        s = self.client.session
        s['user_id'] = 'test'
        s['name'] = 'hany'
        s.save()
        response_post = self.client.post('/', {'id': 'abcd'})
        self.assertEqual(response_post.status_code, 200)

    def test_client_can_delete_and_delete_selected(self):
        title = "hehehe"
        s = self.client.session
        s['user_id'] = 'test'
        s['name'] = 'hany'
        s['book'] = [title]
        s.save()
        response_post = self.client.post('/favorite/cekFav?q=quiliting', {'id': title})
        self.assertEqual(response_post.status_code, 200)

    def test_client_can_get_data(self):
        s = self.client.session
        s['user_id'] = 'test'
        s['name'] = 'hany'
        s['book'] = []
        s.save()
        response = self.client.get('/favorite/cekFav?q=quiliting')
        self.assertEqual(response.status_code, 200)

    # def test_books_already_favorit(self):
    #     s = self.client.session
    #     s['user_id'] = 'test'
    #     s['name'] = 'hany'
    #     s['book'] = []
    #     s.save()
    #     get = requests.get("https://www.googleapis.com/books/v1/volumes?q=quilting").json()
    #     items = get['items']
    #     book = items[0]
    #     dataId = book['id']
    #     self.client.post('/', {'id': dataId})
    #     response = self.client.get('/favorite/cekFav?q=quilting').json()
    #     bool = response['data'][0]['boolean']
    #     self.assertTrue(bool)

    def test_books_not_favorited(self):
        s = self.client.session
        s['user_id'] = 'test'
        s['name'] = 'hany'
        s['book'] = []
        s.save()
        response = self.client.get('/favorite/cekFav?q=quiliting').json()
        bool = response['data'][0]['boolean']
        self.assertFalse(bool)



    # def test_url_page_is_exist(self):
    #     response = Client().get('/favorite/')
    #     self.assertEqual(response.status_code, 200)
    #
    # def test_using_index_function(self):
    #     found = resolve('/favorite/')
    #     self.assertEqual(found.func, favorite)

    # def test_model_can_create_new_favorite(self):
    #     Favorite.objects.create(bookTitle='abc', id='def')
    #     count = Favorite.objects.count()
    #     self.assertEqual(count, 1)

    # def test_client_can_POST_and_render(self):
    #     title = "hehehe"
    #     response_post = Client().post('/favorite/', {'title': title, 'id': 'abcd'})
    #     self.assertEqual(response_post.status_code, 200)
    #
    # def test_client_can_delete_and_delete_selected(self):
    #     title = "hehehe"
    #     Client().post('/favorite/', {'title': title, 'id': title})
    #     response_post = Client().post('/favorite/unfavorite/', {'id': title})
    #     self.assertEqual(response_post.status_code, 200)
    #
    #
    # def test_client_cannot_delete_if_no_id_match(self):
    #     title = "iihhh"
    #     Client().post('/favorite/', {'title': title, 'id': title})
    #     response_post = Client().post('/favorite/unfavorite/', {'id': 'olala'})
    #     self.assertEqual(response_post.status_code, 404)
    #
    # def test_client_can_get_data(self):
    #     response = Client().get('/favorite/cekFav?q=quiliting')
    #     self.assertEqual(response.status_code, 200)
    #
    # def test_books_already_favorit(self):
    #     get = requests.get("https://www.googleapis.com/books/v1/volumes?q=quilting").json()
    #     items = get['items']
    #     book = items[0]
    #     dataId = book['id']
    #     title = book['volumeInfo']['title']
    #     Client().post('/favorite/', {'title': title, 'id': dataId})
    #     response = Client().get('/favorite/cekFav').json()
    #     bool = response['data'][0]['boolean']
    #     self.assertTrue(bool)
    #
    # def test_books_not_favorited(self):
    #     response = Client().get('/favorite/cekFav?q=quiliting').json()
    #     bool = response['data'][0]['boolean']
    #     self.assertFalse(bool)
