from django.urls import path
from .views import favorite
from .views import unfavorite
from .views import getData

urlpatterns = [
    path('', favorite, name="favorite"),
    path('unfavorite/', unfavorite, name="unfavorite"),
    path('cekFav', getData, name="cekFav")
]
