from django.http import JsonResponse
from django.shortcuts import render
from django.shortcuts import get_object_or_404
import requests
# from .models import Favorite
from django.http import JsonResponse
from django.http import HttpResponseRedirect
from django.urls import reverse


# Create your views here.


def favorite(request):
    if 'user_id' not in request.session:
        return HttpResponseRedirect(reverse('login'))
    if request.method == 'POST':
        id = request.POST['id']
        if not 'book' in request.session.keys():
            request.session['book'] = [id]
            size = 1
        else:
            books = request.session['book']
            books.append(id)
            request.session['book'] = books
            size = len(books)
        return JsonResponse({'count': size, 'id': id})
    if 'book' in request.session.keys():
        listbookSession = request.session['book']
    else:
        listbookSession = []
    nama = request.session['name']
    return render(request, 'favorite.html', {'count': len(listbookSession), 'nama': nama})

    # if (request.method == 'POST') :
    #     title = request.POST['title']
    #     book_id = request.POST['id']
    #     if not Favorite.objects.filter(pk=book_id).exists():
    #         Favorite.objects.create(id=book_id, bookTitle=title)
    #     count = Favorite.objects.count()
    #     return JsonResponse({'id': book_id,'count' : count})
    # count = Favorite.objects.count()
    # content = {'count' : count}
    # return render(request, 'favorite.html', content)

def unfavorite(request):
    if 'user_id' not in request.session:
        return HttpResponseRedirect(reverse('login'))
    if request.method == 'POST':
        id = request.POST['id']
        books = request.session['book']
        books.remove(id)
        size = len(books)
        request.session['book'] = books
        return JsonResponse({'count': size})

    # if (request.method == 'POST') :
    #     id = request.POST['id']
    #     hasil = get_object_or_404(Favorite, pk=id)
    #     nama = hasil.bookTitle
    #     hasil.delete()
    #     count = Favorite.objects.count()
    #     content = {'count': count, 'name': nama}
    #     return JsonResponse(content)

def getData(request):
    if 'user_id' not in request.session:
        return HttpResponseRedirect(reverse('login'))
    query = request.GET['q']
    get = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + query).json()
    hasil = get['items']
    newlist = []
    for items in hasil:
        dataId = items['id']
        # data = Favorite.objects.filter(pk=dataId)
        # if not data.exists():
        #     bool = False
        # else:
        #     bool = True

        if dataId in request.session['book']:
            bool = True
        else:
            bool = False
        newdict = {"title": items['volumeInfo']['title'], "authors": items['volumeInfo']['authors'],
                   "published": items['volumeInfo']['publishedDate'],
                   'id': items['id'], 'boolean': bool}
        newlist.append(newdict)
    return JsonResponse({'data': newlist})
